import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from mayavi import mlab

import random

def draw_graph3d(graph, logic, labels, graph_colormap='winter', bgcolor = (1, 1, 1),
                 node_size=0.03,
                 edge_color=(0.8, 0.8, 0.8), edge_size=0.002,
                 text_size=0.016, text_color=(0, 0, 0)):

    H=nx.Graph()

    print graph

    # add edges
    for node, edges in graph.items():
        for edge, val in edges.items():
            if val == 1:
                H.add_edge(node, edge)

    #G=nx.convert_node_labels_to_integers(H)

    G=H

    graph_pos=nx.random_layout(G, dim=3)

    # numpy array of x,y,z positions in sorted node order
    xyz=np.array([graph_pos[v] for v in G])
    print xyz

    # scalar colors
    scalars=np.array(G.nodes())+5
    mlab.figure(1, bgcolor=bgcolor)
    mlab.clf()

    s = logic(xyz[:,0], xyz[:,1], xyz[:,2])

    pts = mlab.points3d(xyz[:,0], xyz[:,1], xyz[:,2], s, scale_factor=node_size, scale_mode='none', colormap="spectral", resolution=20)

   # print G.nodes()
    #exit(1)

    print labels

    i = 0
    for v in labels:
        label = mlab.text(xyz[i][0], xyz[i][1], v, z=xyz[i][2], width=text_size, name=str(i), color=text_color)
        label.property.shadow = True
        i+=1

    pts.mlab_source.dataset.lines = np.array([(int(g[0]),int(g[1])) for g in G.edges()])
    tube = mlab.pipeline.tube(pts, tube_radius=edge_size)
    mlab.pipeline.surface(tube, color=edge_color)

    mlab.show() # interactive window

# graph example
#draw_graph3d({0:{1:1, 2:1}, 1:{0:1, 2:1}, 2:{1:1, 0:1}, 3:{1:1}}, cMap, ['a', 'b', 'c', 'd'])
