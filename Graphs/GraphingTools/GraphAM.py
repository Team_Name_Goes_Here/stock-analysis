#This script will graph an adjacency matrix

"""
Usage: python GraphAM.py <FILE-WITH-ADJ-MAT> <HOW-MANY-NODES-TO-INCLUDE>

We have 922 nodes in our data, but it takes a long time to generate
full graph, and it ends up being cluttered. Somewhere around 120-150 
works well.
"""

from Graphing import *
import sys
import random as rand

sys.path.insert(0, "../DataStripping")

from CSVMethods import *

def spearman_logic(x, y, z):
	return [rand.random() for i in range(len(x))]

def graph_from_matrix(matrix):

	graph = {i:{i:1} for i in range(len(matrix))}

	for i in range(len(matrix)):
		for j in range(len(matrix)):
			if int(matrix[i][j]) == 1:# and i != j:
				graph[i][j] = 1
	return graph

if __name__ == "__main__":

	csvfilename = sys.argv[1]

	stop = int(sys.argv[2])

	adj_matrix = list(get_content(csvfilename))

	AM = [a[1:stop+1] for a in adj_matrix[1:stop+1]]

	#print AM
	
	graph = graph_from_matrix(AM)

	#print graph

	draw_graph3d(graph, spearman_logic, adj_matrix[0][1:stop+1])