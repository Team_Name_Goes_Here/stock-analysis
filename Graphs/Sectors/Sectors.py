"""
Usage: python Sectors.py <SECTOR_NAME>

"""


import sys
sys.path.insert(0, "../DataStripping")
from CSVMethods import *

if __name__ == '__main__':
	
	Sector = sys.argv[1]
	constituents = []

	data = get_content("../Data/Sector_Data/" + Sector + ".csv")
	constituents = [a[0] for a in data[1:]]

	Full_Matrix = get_content("../Data/Spearman_ADJs/Spearman_ADJ_Full_.csv")

	all_stocks = Full_Matrix[0][1:]

	rows_needed = [a for a in Full_Matrix if a[0] in constituents]

	tickers = [a[0] for a in rows_needed]

	prelim_numbers = [all_stocks.index(a[0]) for a in rows_needed]

	updated_rows = [[a[i+1] for i in range(len(a)) if i in prelim_numbers] for a in rows_needed]

	for i in range(len(updated_rows)):
		updated_rows[i].insert(0, tickers[i])

	updated_rows.insert(0, ['Symbol'] + tickers)

	write_data(updated_rows, "../Data/" + Sector + "_ADJ.csv")
