import sys
import os
sys.path.insert(0, "../DataStripping")
from CSVMethods import *

def to_float(f):
	if f == '':
		return 0
	return float(f)

def shuff(adj_mat, constituents):

	Full_Matrix = adj_mat

	all_stocks = Full_Matrix[0][1:]

	rows_needed = [a for a in Full_Matrix if a[0] in constituents]

	tickers = [a[0] for a in rows_needed]

	prelim_numbers = [all_stocks.index(a[0]) for a in rows_needed]

	return rows_needed, prelim_numbers, constituents

def Process(mat, i, j, k):

	rows = range(i,j)
	cols = range(j,k)

	total = 0
	num_cons = 0

	for l in rows:
		for m in cols:
			con = to_float(mat[l][m])
			total += con

			if con != 0:
				num_cons += 1

	return total, num_cons

def scalar_mat_mult(mat, s):
	return [[a*s for a in mat[i]] for i in range(len(mat))]

	

if __name__ == '__main__':
	
	filenames = os.listdir("../Data/Sector_ADJs")
	big_mat = get_content(sys.argv[1])
	constituents = []
	rows_we_need = []
	indices_needed = []
	num_in_each = []
	sectors = [a[:-4] for a in filenames]

	for f in filenames[6:] + filenames[0:6]:
		info = shuff(big_mat, get_content("../Data/Sector_ADJs/" + f)[0][1:])

		num_in_each.append(len(info[0]))

		for a in info[0]:
			rows_we_need.append(a)

		for a in info[1]:
			indices_needed.append(a)

		for a in info[2]:
			constituents.append(a)

	updated_rows = []

	for row in rows_we_need:
		updated_rows.append([row[0]] + [row[i+1] for i in indices_needed])

	updated_rows.insert(0, ['Symbol'] + constituents)
	

	#at this point, the matrix is in updated_rows
	indices = [0]

	so_far = 0
	for i in range(len(num_in_each)):
		indices.append(so_far + num_in_each[i])
		so_far += num_in_each[i]

	matrix = [row[1:] for row in updated_rows[1:]]
	
	######################################################(Actual Alg)
	new_mat = [[0 for i in range(len(sectors))] for j in range(len(sectors))]
	l = len(indices)
	total = 0
	i = 0
	j = 1
	k = 2

	while not j == l-1:
		while k != l:
			D,n = Process(matrix,indices[i],indices[j],indices[k]) #loop in here
			new_mat[i][k-1] = D
			total += n
			k += 1
		i += 1
		j += 1
		k = j+1

	avg_num_conn = float(total)/len(sectors)

	print avg_num_conn

	new_mat = scalar_mat_mult(new_mat, 1.0/avg_num_conn)

	######################################################

	new_mat.insert(0, ['Sector'] + sectors)

	for i in range(0,len(sectors)):
		new_mat[i+1].insert(0,sectors[i])

	write_data(new_mat, "sectors.csv")