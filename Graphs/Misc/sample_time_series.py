import matplotlib.pyplot as plt 
import matplotlib.lines as lns
import numpy as np 
import sys
import csv, datetime

def get_start_date(fileName):

	# Adding a .csv to fileName if need be
	l = fileName.split('.')
	if 'csv' not in l:
		fileName += '.csv'

	stockNames = iter([])
	path_name = "../Data/historicaldata/Details/"
	try:
		stockNames = open(path_name+fileName) #open csv file
	except:
		print(fileName + " doesn't exist") #print this message if file does not exist
		raise Exception('File Not Found')

	namesReader = csv.reader(stockNames) #create a reader for the file

	return  np.array(list(namesReader))[1,1] #get the contents in list form

def convert_date(time_list):
    date_list = np.array([])
    for entry in time_list:
        year = int(entry[:4])
        month = int(entry[5:6])
        day = int(entry[7:])
        t = datetime.datetime(year, month, day, 0, 0)
        print t
        date_list = np.append(date_list, t.strftime('%m/%d/%Y') ) 
    return date_list

# This function will generate a time series plot
# for a given stock
def main(stock_name):
	# open and read TimeSeries.py
	try:
		TimeSeries = open("../Data/TimeSeries.csv")
		read_time_series = csv.reader(TimeSeries)
	except:
		print("Stock name does not exist")

	series = None

	# find the stock one wants to read
	for row in list(read_time_series):
		if str(row[0]) == str(stock_name):
			# Convert all string values to floats
			series = [float(i) if i != '' else 0.0 for i in row[1:]]

	# If the list(read_time_series) is a NoneType
	if series is None:
		print("Stock file does not exist")
		return

	start_date = get_start_date(stock_name)
	# start_date = convert_date(start_date)
	# print 'start_date = ', start_date
	plt.title("Time series of %s"%(stock_name))
	plt.xlabel('time in days starting from Jan 2nd, 2008')
	plt.ylabel('price')

	plt.scatter(range(len(series)), series, s=5,alpha=.5)

	plt.show()

if __name__ == '__main__':
	main(sys.argv[1])