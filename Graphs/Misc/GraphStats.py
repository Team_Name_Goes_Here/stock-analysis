import sys
sys.path.insert(0, "../DataStripping")
from CSVMethods import *

def to_float(f):
    if f == '':
        return 0
    return float(f)

def v_sum(vector, bin=False):
	v_sum = 0

	for v in vector:
		if not bin:
			v_sum += to_float(v)
		else:
			if to_float(v) != 0:
				v_sum += 1


	return v_sum

def degree_dist(adj_mat):
	dist = {i:0 for i in range(len(adj_mat)+1)}

	for a in adj_mat:
		total = v_sum(a, True)
		dist[total] += 1
	return dist	

def edge_dist(adj_mat):
	pos_neg = [-float(i) for i in range(1,11)] + [float(i) for i in range(11)]

	print pos_neg
	dist = {(i/10, (i+1)/10):0 for i in pos_neg if i != 10}

	print dist

	intervals = dist.keys()

	for a in adj_mat:
		for b in a:
			for intv in intervals:
				if to_float(b) <= intv[1] and to_float(b) >= intv[0]:
					dist[intv] += 1

	return dist

def deg_v(adj_mat):
	return [v_sum(a) for a in adj_mat]

if __name__ == '__main__':
	
	adj_mat_prelim = get_content(sys.argv[1])

	adj_mat = [a[1:] for a in adj_mat_prelim[1:]]

	two_v = [adj_mat_prelim[0][1:], deg_v(adj_mat)]

	dist = degree_dist(adj_mat)


	#dist = edge_dist(adj_mat)

	write_data(list(zip(*[dist.keys(), dist.values()])), "../Data/ticker_degrees.csv")

	#write_data()

	#print deg_v(adj_mat)
	#print edge_dist(adj_mat)
	#print degree_dist(adj_mat)
