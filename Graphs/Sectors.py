"""
Usage: python Sectors.py <SECTOR_NAME>

"""


import sys
sys.path.insert(0, "../DataStripping")
from CSVMethods import *

if __name__ == '__main__':
	
	Sector = sys.argv[1]
	constituents = []

	data = get_content("../sector_data.csv")

	#print data 

	constituents = [a[0] for a in data[1:] if a != [] and a[1] == Sector]

	Full_Matrix = get_content(sys.argv[2])

	all_stocks = Full_Matrix[0][1:]

	rows_needed = [a for a in Full_Matrix if a[0] in constituents]

	tickers = [a[0] for a in rows_needed]

	prelim_numbers = [all_stocks.index(a[0]) for a in rows_needed]

	updated_rows = [[a[i+1] for i in range(len(a)) if i in prelim_numbers] for a in rows_needed]

	for i in range(len(updated_rows)):
		updated_rows[i].insert(0, tickers[i])

	updated_rows.insert(0, ['Symbol'] + tickers)

	write_data(updated_rows, sys.argv[3])
