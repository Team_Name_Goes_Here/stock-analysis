import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import sys, os, csv

# Plots graph from adjacency matrix.
def show_graph(adjacency_matrix):
    # Get edges from adj. matrix.
    rows, cols = np.where(adjacency_matrix != 0)
    edges = zip(rows.tolist(), cols.tolist())
    
    gr = nx.Graph()
    gr.add_edges_from(edges)

    # Get list of subgraph nodes
    sub_graphs = list(nx.connected_components(gr)) 

    #Layout of nodes
    pos = nx.shell_layout(gr) 
    nx.draw_networkx(gr,pos, node_color="deepskyblue", edge_alpha = 0.8)
    plt.show()

# finds the theta file we are looking for
# returns the adjacency matrix
def make_adj(theta):
    file_name = ''
    thresholds = os.listdir('../Data/theta')

    for files in thresholds:
        if theta in files:
            file_name = files

    try:
        file_obj = open('../Data/theta/'+file_name, 'rb')
        file_reader = csv.reader(file_obj)
    except Exception, e:
        raise Exception('File not found')

    adj = np.array(list(file_reader))
    adj = adj.astype(np.float)

    for x in xrange(len(adj)):
        row = adj[x]
        for y in xrange(len(row)):
            if x == y:
                adj[x,y] = 0.0
    print adj

    return adj

# returns a degree vector for a given 
# adjacency matrix
def get_deg(adj):
    
    degree = np.array([])
    
    for row in adj:
        deg = 0
        for i in row:
            if i != 0:
                deg += 1
        degree = np.append(degree, deg)

    return degree


def main(theta):
    adj = make_adj(theta)
    deg = get_deg(adj)
    non_zero_degrees = [int(i) for i in deg if i != 0]

    print non_zero_degrees, len(non_zero_degrees)
    show_graph(adj)

if __name__ == '__main__':
    main(sys.argv[1])