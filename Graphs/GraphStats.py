import sys
sys.path.insert(0, "../DataStripping")
from CSVMethods import *

def to_float(f):
    if f == '':
        return 0
    return float(f)

def v_sum(vector, bin=False):
	v_sum = 0

	for v in vector:
		if not bin:
			v_sum += to_float(v)
		else:
			if to_float(v) != 0:
				v_sum += 1


	return v_sum

def degree_dist(adj_mat, bin=False):
	dist = {i:0 for i in range(len(adj_mat)+1)}

	for a in adj_mat:
		total = v_sum(a, bin)
		dist[total] += 1
	return dist	

def edge_dist(adj_mat):
	pos_neg = [-float(i) for i in range(1,11)] + [float(i) for i in range(11)]

	#print pos_neg
	dist = {(i/10, (i+1)/10):0 for i in pos_neg if i != 10}

	#print dist

	intervals = dist.keys()

	for a in adj_mat:
		for b in a:
			for intv in intervals:
				if to_float(b) <= intv[1] and to_float(b) >= intv[0]:
					dist[intv] += 1

	return dist

def deg_v(adj_mat,  bin=False):
	return [v_sum(a,bin) for a in adj_mat]

if __name__ == '__main__':
	
	adj_mat_prelim = get_content(sys.argv[1])

	adj_mat = [a[1:] for a in adj_mat_prelim[1:]]

	two_v = [adj_mat_prelim[0][1:], deg_v(adj_mat, True)]

	print two_v[0][0]

	two_v[0].insert(0,"Ticker")
	two_v[1].insert(0,"Degree")

	dist = edge_dist(adj_mat)
	dist_list = [[a[0],a[1]] for a in zip(*[dist.keys(), dist.values()])]

	dist_list.insert(0,("Interval","Num Edges"))
	#dist = edge_dist(adj_mat)

	deg_dist = degree_dist(adj_mat, True)
	deg_dist_vs = [[a,deg_dist[a]] for a in deg_dist.keys()]

	deg_dist_vs.insert(0,["Degree", "Num_Occurs"])

	write_data(dist_list, sys.argv[2])
	write_data([[a[0], a[1]] for a in zip(*two_v)], sys.argv[3])
	write_data(deg_dist_vs, sys.argv[4])

	#write_data()

	#print deg_v(adj_mat)
	#print edge_dist(adj_mat)
	#print degree_dist(adj_mat)
