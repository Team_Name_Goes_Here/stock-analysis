=================================================================================
                    Here is how you can add to this repository
=================================================================================

Establish the repository on your computer with

	git clone https://<YOUR-USERNAME-HERE>@bitbucket.org/Team_Name_Goes_Here/stock-analysis.git

Once the repository is on your computer you can edit and add stuff.
To add to the repository:

	git add <file_name> (or "git add -A" if you updated multiple files)
	git commit -m "<message that shows what edits occured>"
	git push

Last thing to know: If the repository was edited while you were away and you want update the folder.
	
	git pull

Make sure you have a BitBucket account in order to pull off the above commands.


=================================================================================
                       Plotting a time series for a stock
=================================================================================

Make sure you are in the Graphs folder, then run 
        
	python sample_time_series.py <STOCK_NAME>

=================================================================================
			Using Spearman-Rank algorithm 
=================================================================================

One must be in the DataStripping folder, then run

        python Correlations.py <STOCK_NAME1> <STOCK_NAME2>

=================================================================================
		     Creating the Spearman Adjacency matrix 
=================================================================================

One time run process, already done. To generate again, must be in the 
Processing folder, then run

        python ADJ.py
        python BH.py ../Data/Spearman_ADJ.csv

The matrix will be in Data/Spearman_ADJ_Full_.csv

=================================================================================
		       k-Core Analysis and theta-threshold
=================================================================================

We should produce new adjacency matrices based off a theta threshold, 
where we could do k-core analysis off of those.

To make an adjacency matrix of a certain theta threshold value,

Be in the Processing folder and run the following command:

        python thetaThreshold.py <THETA_VALUE>

Notes: 
 - THETA_VALUE is a float number between (and including) 0.0 and 1.0.
 - The first time you run it, if the file does not already exist, it will crash 
    and create the file for you.
 - The second time you run it, it will populate the file previously made with 
    information from Spearman_ADJ_Full.csv.

You can find the csv file produced in StockAnalysis/Data/theta/ folder.

=================================================================================
		           Enforcing a Theta Threshold
=================================================================================

Navigate to the DataStripping folder, then run

        python ET.py <PATH-TO-MATRIX> <THETA>

An adjacency matrix of 0s and 1s will be at Data/<ORIGINAL-NAME>_<THETA>_.csv

=================================================================================
		           Graphing An Adjacency Matrix
=================================================================================

Go to the Graphs folder, then run

        python GraphAM.py <PATH-TO-ADJ-MATRIX> <NUM-OF-NODES>

You will see a window pop up, graphing 100 nodes should take about 2 mins

=================================================================================
		           Degree Distribution Maker
=================================================================================

Be in the Processing folder, then run
		
		python deg_dist_maker.py <PATH_NAME_TO_FILE>

This will read the Deg_Vec.csv in the appropriate folder and 
generate a .png file inside the same folder.

=================================================================================
		           Frequency Distribution Maker
=================================================================================

Be in the Processing folder, then run
		
		python freq_dist_maker.py <PATH_NAME_TO_FILE>

This will read the Edge_Dist.csv in the appropriate folder and 
generate a .png file inside the same folder.

=================================================================================
		           	Shifting time series
=================================================================================

Be in the root folder, then run

		python Shift.py <STOCK_TICKER1> <STOCK_TICKER2>

This will print a list of 2-tuples that consist of the shift in time
series with its corresponding Spearman Rank correlation coefficient.