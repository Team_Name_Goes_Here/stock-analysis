import sys, csv
import numpy as np
import pandas as pd 
# sys.path.insert(0,'/Data')

def get_content(fileName):

	#print fileName

	# Adding a .csv to fileName if need be
	l = fileName.split('.')
	if 'csv' not in l:
		fileName += '.csv'

	stockNames = iter([])
	#path_name = "../Data/historicaldata/Details/"
	try:
		stockNames = open(fileName) #open csv file
	except:
		print(fileName + " doesn't exist") #print this message if file does not exist
		raise Exception('File Not Found')

	namesReader = csv.reader(stockNames) #create a reader for the file

	return  np.array(list(namesReader)) #get the contents in list form

def intersection_length(s1, s2):
    content1 = get_content(s1)
    content2 = get_content(s2)

    t1 = np.array(map(int, content1[1:,1]))
    t2 = np.array(map(int, content2[1:,1]))

    # p1 = content1[2:,16]
    # p2 = content2[2:,16]

    # t_and_p1 = zip(t1,p1)
    # t_and_p2 = zip(t2,p2)
    intersect_times = np.intersect1d(t1, t2)

    return len(intersect_times)