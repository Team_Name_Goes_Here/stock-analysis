#This script will run analyis on thetas of 0.1, 0.2, 0.3, ... , 0.9, and 0.95
#The results of the analysis will be put into the Analysis folder
#found in the root directory
#USAGE: ./analysis.bash

#Here we will create the matrix for each threshold,
#logical and non-logical

thetas=(0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 0.95)
sectors=('Basic_Materials' 'Conglomerates' 'Consumer_Goods' 'Financial' 'Healthcare' 'Industrial_Goods' 'Services' 'Technology' 'Utilities')

if [[ $1 == --init ]]; then
	mkdir Analysis/2008-9
	mkdir Analysis/2010-11
	mkdir Analysis/2012-13
	mkdir Analysis/2014-15
	mkdir Analysis/2008-15

	for x in Analysis/*; do 
	 	for theta in ${thetas[*]}; do
	 		mkdir $x/Theta$theta
	 		touch $x/Theta$theta/ADJ.csv
			touch $x/Theta$theta/Logical_ADJ.csv
			touch $x/Theta$theta/Deg_Vec.csv
			touch $x/Theta$theta/Edge_Dist.csv
			touch $x/Theta$theta/kcore_ADJ.csv
	 	done
	 done

elif [[ $1 == --dists ]]; then
	cd Graphs
	for x in Analysis/*; do 
		for y in ${thetas[*]}; do
				echo ../$x/Theta$y/ADJ.csv
				python GraphStats.py ../$x/Theta$y/ADJ.csv ../$x/Theta$y/Edge_Dist.csv ../$x/Theta$y/Deg_Vec.csv
		done
	done
	cd ..

elif [[ $1 == --mats ]]; then
	for x in Analysis/*; do 
		for y in ${thetas[*]}; do
			cd Processing
			python BH.py ../$x/SR_ADJ.csv ../$x/Theta$y/ADJ.csv
			python BH.py ../$x/SR_ADJ.csv ../$x/Theta$y/Logical_ADJ.csv
			cd ../DataStripping
			python ET.py ../$x/SR_ADJ.csv $y ../$x/Theta$y/ADJ.csv
			cd .. 
		done
	done

elif [[ $1 == --plots ]]; then
	cd Processing
	for x in Analysis/*; do 
		for y in ${thetas[*]}; do
			python deg_dist_maker.py ../$x/Theta$y/Deg_Vec.csv
			python freq_dist_maker.py ../$x/Theta$y/Edge_Dist.csv
		done
	done
	cd ..

elif [[ $1 == --sectors ]]; then
        for x in Analysis/*; do
                for y in ${sectors[*]}; do
			cd Graphs
                python Sectors.py "$y" ../$x/SR_ADJ.csv ../$x/Sector_ADJs/$y.csv
			cd ..
		done
		cd Graphs
		python graph_sectors.py ../$x/SR_ADJ.csv ../$x/Sector_ADJs/ ../$x/sectors.csv
		cd ..

		cd Processing
		python BH.py ../$x/sectors.csv
		cd ..
        done

elif [[ $1 == --all ]]; then
	for x in Analysis/*; do 
		for y in ${thetas[*]}; do
			cd DataStripping
			python ET.py ../$x/SR_ADJ.csv $y ../$x/Theta$y/ADJ.csv --nbin
			python ET.py ../$x/SR_ADJ.csv $y ../$x/Theta$y/Logical_ADJ.csv --bin
			cd .. 
			cd Processing
			python BH.py ../$x/Theta$y/ADJ.csv
			python BH.py ../$x/Theta$y/Logical_ADJ.csv

			cd ../Graphs
			echo ../$x/Theta$y/ADJ.csv
			python GraphStats.py ../$x/Theta$y/ADJ.csv ../$x/Theta$y/Edge_Dist.csv ../$x/Theta$y/Deg_Vec.csv ../$x/Theta$y/Deg_Dist.csv
			
			cd ../Processing
			python deg_dist_maker.py ../$x/Theta$y/Deg_Vec.csv
			python freq_dist_maker.py ../$x/Theta$y/Edge_Dist.csv
			cd ..
		done
	done
fi
