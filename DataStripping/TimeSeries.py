import csv
import os
from CSVMethods import Methods as csvm

# RETX column is the 16th column in 0-based indexing
# first step: make the time series set for one stock
# read from csv file from an arbitrary column to create the time series for one stock
# put this in an array
# sort the array (account for ties)
# repeat for another stock

# This script is meant to write to one file the name of the stock and the corresponding time series 
def main():

	pathname = "../Data/historicaldata/Details/"
	stocknames = os.listdir(pathname)

	timeSeriesFile = open('../Data/TimeSeries.csv', 'w', newline='')
	timeSeriesWriter = csv.writer(timeSeriesFile)

	# Look at every stock.csv file
	for stock in stocknames:

		# Read from the file
		try:
			stockData = open(pathname+stock)
			stockReader = csv.reader(stockData)
		except:
			print("Could not open: " + pathname + stock)

		timeSeries = csvm.get_col(12, pathname+stock)[1:]

		timeSeries.insert(0, stock[::-1][4:][::-1])

		timeSeriesWriter.writerow(timeSeries)

		if not stockReader.closed: stockReader.close()

	if not timeSeriesWriter.closed: timeSeriesWriter.close()


main()