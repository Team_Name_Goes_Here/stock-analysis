from SpearmanCorrCalc import get_rank_of
import unittest

# This file will be used to test each section of calculating
# Spearman Rank Correlation Coefficient.

# The main section of the code that needs testing is
# the get_rank_of() method in SpearmanCorCalc.py
# We must ensure that we get the correct rank 
# for any input time series
class SpearmanTestCases(unittest.TestCase):

	def setUp(self):
		stock_name = "<PLACE STOCK NAME HERE>"

	def test_get_rank_of(self):
		# Create a dummy time series
		dummy_series = range(20)
		# manually calculate the rank
		rank_of_dummy = range(1,21)
		# check that the algorithm ouputs the same rank
		self.assertEqual(rank_of_dummy, get_rank_of(dummy_series))

	def test_get_rank_of_with_different_length_time_series(self):
		# test for different length time series
		pass


# ====================================
#    Bugs should be put in here too
# ====================================

if __name__ == '__main__':
	unittest.main()