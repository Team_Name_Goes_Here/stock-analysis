import csv
import sys
import numpy as np
		
def get_content(fileName):

	#print fileName

	# Adding a .csv to fileName if need be
	l = fileName.split('.')
	if 'csv' not in l:
		fileName += '.csv'

	stockNames = iter([])
	#path_name = "../Data/historicaldata/Details/"
	try:
		stockNames = open(fileName) #open csv file
	except:
		print(fileName + " doesn't exist") #print this message if file does not exist
		raise Exception('File Not Found')

	namesReader = csv.reader(stockNames) #create a reader for the file

	return  list(namesReader) #get the contents in list form


def write_data(data, fileName):
    l = fileName.split('.')
    if 'csv' not in l:
        fileName += '.csv'

    stockNames = iter([])
    #path_name = "../Data/historicaldata/Details/"
    try:
        stockNames = open(fileName,'w') #open csv file
    except:
        print(fileName + " doesn't exist") #print this message if file does not exist
        raise Exception('File Not Found')

    namesWriter = csv.writer(stockNames) #create a reader for the file
    
    for d in data:
        namesWriter.writerow(d)














