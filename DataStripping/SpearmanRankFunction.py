import random
import math
import csv 
from CSVMethods import *
import numpy as np

# This code takes an unsorted list (DO NOT SORT IT) of data, 
# ranks it and then resorts it in the original order.
def rank_log_returns(unsorted_list):
    index_retainer = [(unsorted_list[i],i) for i in range(len(unsorted_list))] #Stores original index of unsorted values.
    if not index_retainer:
        return
    sorted_list, original_positions = zip(*sorted(index_retainer)) #Assigns sorted list and a list of original positions.

    rank = []
    main_placeholder = 0
    for i in range(1,len(sorted_list)+1): #i is rank if no values are equal

        #skip loop if added from previously repeated values
        if len(rank) > main_placeholder:
            main_placeholder+=1
            continue 

        try: 
            #Check if last entry
            sorted_list[main_placeholder+1]
        except IndexError:
            rank.append(float(i)) #last list entry
            break

        #not equal to any then add rank to list
        if sorted_list[main_placeholder] != sorted_list[main_placeholder+1]:
            rank.append(float(i)) 
        else:
            times_in_list = 1
            try:
                while sorted_list[main_placeholder] == sorted_list[main_placeholder+times_in_list]:
                    times_in_list+=1
            except IndexError:
                pass #Made it to the end of the list.
            finally:
                cum = 0
                #Get sum of ranks of equal values
                for j in range(times_in_list): 
                    cum+=i
                    i+=1
                #append averaged rank
                for k in range (times_in_list):
                    rank.append(float(cum/times_in_list)) 

        main_placeholder+=1

    index_restore = [(original_positions[i],rank[i]) for i in range(len(unsorted_list))]
    #Restore positions
    pos, ordered_ranks = zip(*sorted(index_restore)) 
    return ordered_ranks

#Takes price list and returns list of log returns.
def log_returns(price_list): 
    #print price_list
    log_return = []
    # for i in range(len(price_list)-1):
    #     lr = np.log(abs(price_list[i+1] / np.array(price_list[i])))
    #     log_return.append(lr)
    for i in range(len(price_list)):
        if abs(price_list[i]) == 0:
            lr = 0
        else:
            lr = np.log(abs(price_list[i]))
        log_return.append(lr)
    return log_return


#Computes simple correlation coefficient for Spearman Rank
def simple_rank_test(s1,s2): 
    N = len(s1)
    D = [s1[i]-s2[i] for i in range(N)]
    numerator = sum([D[i] * D[i] for i in range(N)])
    denominator = N * (N ** 2 - 1)
    rho = 1 - 6 * (numerator / denominator) 
    return rho

#Computes rho from 2 ranked lists.
def complex_rank_test(s1,s2):
    
    s1_bar = sum(s1)/len(s1)
    s2_bar = sum(s2)/len(s2)

    d1 = [i - s1_bar for i in s1] #List RiX - RXbar
    d2 = [i - s2_bar for i in s2] #List RiY - RYbar

    numerator_product = [a * b for a,b in zip(d1,d2)] #Multiply each element in list by corresponding element in other list.

    d1_squared = [i ** 2 for i in d1] #List (RiX - RXbar)^2
    sum_d1_squared = sum(d1_squared)

    d2_squared = [i ** 2 for i in d2] #List (RiY - RYbar)^2
    sum_d2_squared = sum(d2_squared)

    numerator = sum(numerator_product)
    denominator = math.sqrt(sum_d1_squared * sum_d2_squared)
    if denominator == 0:
        return None
        
    rho = float(numerator/denominator)
    return rho


# Checks if a list is monotonically increasing
def is_monotonically_increasing(a_list):
    for i in range(len(a_list)-1):
        if a_list[i] > a_list[i+1]:
            return False
    return True

# look into np.loadtxt
def date_pull(s1, s2, start_year='2007', end_year='2016'):

    content1 = get_content(s1)
    content2 = get_content(s2)

    # Specifying what years we want
    date_col1 = content1[1:,1]
    date_col2 = content2[1:,1]

    indeces_needed1 = [i for i in xrange(len(date_col1)) if start_year <= date_col1[i][:4] and  end_year >= date_col1[i][:4] ]
    indeces_needed2 = [i for i in xrange(len(date_col2)) if start_year <= date_col2[i][:4] and  end_year >= date_col2[i][:4] ]

    retx_col1 = content1[indeces_needed1]
    retx_col2 = content2[indeces_needed2]

    t1 = np.array(map(int, content1[1:,1])) #times
    t2 = np.array(map(int, content2[1:,1]))

    p1 = retx_col1[2:,16]
    p2 = retx_col2[2:,16]

    # t_and_p1 = zip(t1,p1)
    # t_and_p2 = zip(t2,p2)

    intersect_times = np.intersect1d(t1, t2)
    
    if not intersect_times.any():
        return [], [], []

    if len(intersect_times) < 365*float(int(end_year)-int(start_year))/8.0:
        return [], [], []

    #Modeling assumption: Eliminate stocks with too few intersection dates.
    # if intersect_times.size < 10:
    #     return []

    # start = 0 
    # end = 0
    # if any(intersect_times):
    p1, p2 = trim(p1, p2)   
    # print p1,p2
    start = intersect_times[0]
    end = intersect_times[-1]
    

    index1 = np.logical_and(start<=t1, end >= t1)
    index2 = np.logical_and(start<=t2, end >= t2)

    return intersect_times, p1, p2

def trim(p1, p2):

	l = len(p2) if len(p1) > len(p2) else len(p1)
	indices = range(l)

	a = [p1[i] for i in indices if is_num(p1[i]) and is_num(p2[i])]
	b = [p2[i] for i in indices if is_num(p2[i]) and is_num(p1[i])]

	return (np.asarray(a),np.asarray(b))

def is_num(some_str):

	if some_str == '':
		return False

    	if type(some_str) != str:
		return True
        
	for s in some_str:
		if not (s.isdigit() or s == '.'):
			return False
	return True


#TEST THE FUNCTION
if __name__ == '__main__':


	a = np.array([1,'',3,'',5])
	b = np.array([1,2,3,4,5,'',4,4,4])
	# print trim(a,b)[0]

# c,d,e = date_pull('../Data/historicaldata/Details/COKE', '../Data/historicaldata/Details/BODY')
# print c,d,e 

	
    # random_data_1 = []
    # random_data_2 = []
    # for i in range(100):
    #     #random_data.append(random.randint(1,50))
    #     random_float_1 = random.random()*2-1
    #     random_data_1.append(random_float_1)
    #     random_float_2 = random.random()*2-1
    #     random_data_2.append(random_float_2)
    #     if random.randint(0,5) == 3:
    #         random_data_1.append(random_float_1)
    #         random_data_2.append(random_float_2)

    # print('-------------------Data--------------------')
    # print(random_data_1)
    # # print('-------------------Log Returns--------------------')
    # # print(log_returns(sorted_data))
    # print('-------------------Ranks--------------------')
    # print(rank_log_returns(random_data_1))
    # print(len(random_data_1))

    # rank1 = rank_log_returns(random_data_1)
    # rank2 = rank_log_returns(random_data_2)
    # print('Rho:',complex_rank_test(rank1,rank2))
    # t, p1, p2 = date_pull('AAC', 'CAU')
    # print t, p1, p2
    # p1 = [float(i) for i in p1]
    # print sorted(log_returns(p1))

    
