import sys
sys.path.insert(0,'../DataStripping')
from CSVMethods import *

"""
This script enforces the threshold on a set of data.

Usage: python ET.py <FILENAME> <THRESHOLD>

Data will be written to <FILENAME>_<THRESHOLD>_.csv
"""

def to_float(f):
    if f == '':
        return 0
    return float(f)

if __name__ == "__main__":

    content = get_content(sys.argv[1])
    threshold = float(sys.argv[2])

    onesandzeros = []
    onesandzeros.append(content[0])

    for i in range(1, len(content)):
        onesandzeros.append([])
        onesandzeros[i].append(content[0][i])
        for j in range(1, len(content[i])):
	        if to_float(content[i][j]) <= threshold:
	            onesandzeros[i].append(0)
	        else:
			if sys.argv[4] == "--nbin":
				onesandzeros[i].append(to_float(content[i][j]))
			elif sys.argv[4] == "--bin":
				onesandzeros[i].append(1)

    write_data(onesandzeros, sys.argv[3])
    print "Done"

