from SpearmanRankFunction import rank_log_returns, log_returns
from CSVMethods import get_col
from itertools import groupby
import csv
import sys
# import numpy as np

# This function will sort and order the time series of a stock
# returns the rank of a stock in list form
def get_rank_of(stock_name):

	# open and read TimeSeries.py
	try:
		TimeSeries = open("../Data/TimeSeries.csv")
		read_time_series = csv.reader(TimeSeries)
	except:
		print("Stock name does not exist")

	# Time Series Processing
	series = None 			# the time series
	lr_series = None 		# the log return of the time series
	sorted_series = None 	# the sorted log returns

	# find the stock one wants to read
	for row in list(read_time_series):
		if str(row[0]) == str(stock_name):
			# Convert all string values to floats
			series = [float(i) for i in row[1:]]

			# log return of Time series
			# lr_series = log_returns(series)

	# If the list(read_time_series) is a NoneType
	if series is None:
		print("Stock file does not exist")
		return

	rank = rank_log_returns(series)

	return rank


if __name__ == '__main__':
	get_rank_of(sys.argv[1])
