# To scrape web of stock's sectors
import sys, os, csv
import numpy as np
from bs4 import BeautifulSoup as bs 
import urllib2 as url 
#sys.path.insert(0,'/Data/historicaldata/Details/')
sys.path.insert(0,'/DataStripping')
from CSVMethods import *

if __name__ == '__main__':

	sector_list = ['Basic Materials','Conglomerates','Consumer Goods','Financial','Healthcare','Industrial Goods','Services', 'Technology', 'Utilities']

	# List all file names (stockname.csv) in Details folder
	details = "../Data/historicaldata/Details/"
	snames = os.listdir(details)
	total = float(len(snames))

	table = [["TICKER","SECTOR"]]

	counter = 0
	for stock in snames:
		page = url.urlopen('https://finance.yahoo.com/q/in?s=%s+Industry'%stock[:-4])
		soup = bs(page,'html.parser')
		for sector in sector_list:
			if '>'+sector+'</a>' in str(soup):
				table.append([stock[:-4], sector])
				print "Stock %s is in Sector %s"%(stock[:-4], sector)
			else:
				table.append([stock[:-4], "NONE"])

	write_content(table, "sector_data.csv")

