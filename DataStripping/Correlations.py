import random
import math
import sys
from SpearmanRankFunction import *
import numpy as np
# from SpearmanCorrCalc import get_rank_of

class Correlations:

	def __init__(self):
		pass

	def random_cor(Stock1, Stock2):
		return random.uniform(.5,1.5)

	def to_float(self, p):
		if p == 'C':
			return '0'
		return float(p)
		
	# Consider adding the boolean parameter is_simple to 
	# indicate the difference between simple or complex algorithm
	def spearman_cor(self, Stock1, Stock2=None, start=None, end=None, is_simple=False):
		# Here s1 and s2 are the rank lists of the sorted time series
		# of stock1 and stock2
		if Stock2 == None:
			Stock2 = Stock1

		# Now to address the issue of same length ranks.

		# This function returns the time list for which 2 stocks exist
		# at the same time as well as the prices of each stock for 
		# date in the intersection list
		t, p1, p2 = date_pull(Stock1, Stock2, start, end)

		# print(p1, p2)
		if t is None or p1 is None or p2 is None:
			return 0

		# print p2
		p1 = np.array(map(lambda x: self.to_float(x), p1[1:]))
		p2 = np.array(map(lambda x: self.to_float(x), p2[1:]))


		# lr1 = log_returns(p1)
		# lr2 = log_returns(p2)
		
		s1 = rank_log_returns(p1)
		s2 = rank_log_returns(p2)
		
		
		if s1 is None or s2 is None:
			return 0
		s1, s2 = trim(s1,s2)
		# Unmatching lengths of ranks
		try:
			len(s1)
		except Exception, e:
			print('Stocks may not exist at the same time')

		if len(s1) != len(s2):
			print("Ranks of stocks do not match in length")

		if is_simple:
			rho = simple_rank_test(s1,s2)
		else:
			rho = complex_rank_test(s1,s2)
			

		return rho

	def our_cor(Stock1, Stock2):
		print(Stock1.name + ',' + Stock2.name)
		numMissingDataPoints = 0
		
		corTotal = 0

		dVList1 = get_delta_vol(Stock1.get_vol_list())
		dVList2 = get_delta_vol(Stock2.get_vol_list())

		dPList1 = get_delta_price(Stock1.get_price_list())
		dPList2 = get_delta_price(Stock2.get_price_list())

		for i in range(0,min(len(dVList1), len(dVList2)) - 1):
			if(dPList2[i] == 0 or (dVList1[i] + dVList2[i]) == 0):
				numMissingDataPoints += -1
				continue
	
			corTotal += 2*(1/(((dPList1[i]/dVList1[i]) + (dPList2[i]/dVList2[i]))))*((dPList1[i] + dPList2[i])/(dVList1[i] + dVList2[i]))

		if(len(dVList1) == 0):
			return 0

		return corTotal/(len(dVList1) - 1 + numMissingDataPoints)


# ===================================================
#  These helper methods should go inside the class 
# ===================================================

# The following are helper methods for the above correlations

def get_delta_vol(volList):
	dVList = []
	for i in range(0, len(volList) - 1):
		if(i+1 < len(volList)):
			dVList.append(abs((float(volList[i+1]) - float(volList[i]))/float(volList[i])))

	return dVList

def get_delta_price(priceList):
	return get_delta_vol(priceList)

# Main method will calculate the Spearman
# Rank correlation between input stock names
# NOT COMPLETE
def main(s1, s2):
	cor = Correlations()
	rho = cor.spearman_cor(s1,s2)
	print(rho)

if __name__ == '__main__':
	main(sys.argv[1], sys.argv[2])


