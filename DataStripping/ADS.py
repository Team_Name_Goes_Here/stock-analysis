# Aggregate Data Separation
# This sorts David's Data

import csv
import os


def main():
	filename = "../Data/DetailData.csv"
	try:
		stockData = open(filename) # open the file
	except:
		print("something went wrong")

	tickerReader = csv.reader(stockData) # created a reader for the data
	content = list(tickerReader) # gets the content in list form

	column_names = content[0]

	current_ticker = content[1][3]
	path = "../Data/historicaldata/Details/"
	fileTop = True

	outfile = None
	outwriter = None

	for row in content[1:]:

		if row[3] != current_ticker:
			current_ticker = row[3]
			fileTop = True
			if not outfile.closed:
				outfile.close()

		try:
			if fileTop:
				outfile = open(path+current_ticker+".csv", 'w', newline='')
				outwriter = csv.writer(outfile)
				outwriter.writerow(column_names)
				outwriter.writerow(row)
				fileTop = False
			else:
				outwriter.writerow(row)

		except:
			print("can't open file")

	print("done")

main()

	
