from CSVMethods import CSVMethods
from Stats import Stats
from Stock import Stock
from Correlations import Correlations

import csv

names = [] #Holds tickers
historicalprices = {} #Holds all stocks and their historical prices
historicalvolume = {} #Holds all stocks and their historical prices
stocks = [] #list of stocks

names = CSVMethods.get_col(0,'../Data/constituents.csv') #get list of stock names

#This for loop gets historical prices for each stock
for j in range(1,len(names)):
	fileName = '../Data/historicaldata/' + names[j] + '.csv'
	historicalprices[names[j]] = CSVMethods.get_col(5,fileName) #Stores data into dictionary
	historicalvolume[names[j]] = CSVMethods.get_col(6,fileName) #Stores data into dictionary

currentNames = list(historicalprices.keys()) #names of stocks we have data for

#this loop stores stock objects into stocks
#if we don't have data for a particular stock,
#an empty object is put into the list
for k in range(1,len(names)):
	#if we have data for the stock
	if(names[k] in currentNames):
		stocks.append(Stock(names[k], historicalvolume[names[k]], historicalprices[names[k]]))
	#if we don't have data
	else:
		stocks.append(names[k], None, None)

outputFile = open('../Data/Correlations.csv', 'w', newline='') #open results file for writing
outputWriter = csv.writer(outputFile) #create file writer

outputWriter.writerow(names) #write top row, just all tickers

#this loop calculates every stock's
#correlation to every other stock
#and writes results to csv file
for s in stocks:
	results = [] #row of results to be written
	results.append(s.get_name())

	for t in stocks:
		#if(s.equals(t)):
			#results.append(1)
		#else:
		results.append(Correlations.our_cor(s,t))

	outputWriter.writerow(results)

















