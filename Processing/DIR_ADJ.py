# Here we create the directed graph
import sys, csv
import pandas as pd 
import numpy as np 
# sys.path.insert(0, '../DataStripping')
from Shift import *

# create a panda structure that is the directed graph adjacency matrix

# need to make the directed adjacency matrix

# iterate through the matrix and populate it with 
# the appropriate influential shift

# print db

def get_TICKERS():
	return sorted(list(db.TICKER.unique()))[1:]

def influence(s1, s2):
	tup = dir_cor(s1, s2)
	if tup == (0,0):	return 0
	if abs(tup[0][1]) > abs(tup[1][1]):
		return tup[0][0]
	else:
		return tup[1][0]

# make adj matrix of zeros of size m x m
def main():
	# a list of all ticker names
	tickers = get_TICKERS()

	# size of the adj
	m = len(tickers)

	adj = np.zeros((m, m))
	count = 0.0
	total = m**2

	# populating the adjacency matrix
	x,y = 0,0
	for i in tickers:
		for j in tickers:
			adj[x][y] = influence(i,j)

			if count%100 == 0: print 'complete %.2f '%(count/total) 
			count += 1
			y += 1
		x += 1

	outfile = open('../DIR_ADJ.csv','w')
	outwriter = csv.writer(outfile)
	outwriter.writerows(adj)


if __name__ == '__main__':
	main()