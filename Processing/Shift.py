import numpy as np 
import pandas as pd 
from operator import itemgetter
import sys
sys.path.insert(0,'../DataStripping/')

# This file will act as the wrapper that will shift a time series
# to find the greatest correlation and its corresponding shift

# The method we will need in this script
from SpearmanRankFunction import complex_rank_test

def get_db():
	ts = pd.read_csv('../Data/DetailData.csv', low_memory=False)
	db = ts[['TICKER','date','RETX']]
	return db

# Get the relevant information once as a global variable
db = get_db()

def get_time_series(stock):
	return db[db['TICKER'] == stock]

# returns an intersection panda structure of the retx values 
# for two time series for when they exist at the same time
def intersection_lists(db1, db2):
	intersection_db = pd.merge(db1, db2, how='inner', on=['date'])
	if intersection_db.empty:		return [], []
	return intersection_db['RETX_x'], intersection_db['RETX_y']

# clean function to get rid of non-number types in lists
# and convert lists into float types
def clean(l1,l2):
	# if l1.empty or l2.empty:		return [] 
	f = [a for a in zip(l1,l2) if a[0] != 'C' and a[1] != 'C']
	if not f:
		return [], []
		raise Exception('empty lists: ruby wrote this')
	a,b = zip(*f)
	return [float(i) for i in a], [float(i) for i in b]

# the shift will return a list of tuples that represent the shift
# in retx values and their corresponding correlation coefficient
def shift(ret1, ret2):
	if not ret1 or not ret2: 	return []
	rhos = [(i, complex_rank_test(ret1[i:i-10], ret2[:-10])) for i in xrange(10) if len(ret1) > 0 and len(ret2) > 0]
	return rhos

# main method: a wrapper that uses all of the above functions
# to return the correlation list
def wrapper(s1,s2):
	a = get_time_series(s1)
	b = get_time_series(s2)

	ret1, ret2 = intersection_lists(a, b)
	ret1, ret2 = clean(list(ret1), list(ret2))
	rhos = shift(ret1, ret2)
	if not rhos: 	return []
	return rhos

# returns tuple of the maximum and minimum correlations and their shifts
def dir_cor(s1, s2):
	rhos = wrapper(s1, s2)
	if not rhos:	return (0,0)
	return max(rhos, key=itemgetter(1)), min(rhos, key=itemgetter(1))

if __name__ == '__main__':
	wrapper(sys.argv[1], sys.argv[2])

