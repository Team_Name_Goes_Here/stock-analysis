import matplotlib.pyplot as plt 
import numpy as np 
import pandas as pd 
import sys, csv
from ast import literal_eval

# Given a csv file that has the freq vector,
# create the freq distribution 

def make_freq_dist(path):
	try:
		edge_dist = pd.read_csv(path)
	except Exception, e:
		raise e

	edge_dist = edge_dist.sort('Interval')
	intervals = [literal_eval(i) for i in list(edge_dist['Interval'])]
	num_edges = list(edge_dist['Num Edges'])
	total = sum(num_edges)
	freq_dist = [float(edge)/total for edge in num_edges]

	# Labeling plots
	plt.title('Frequency distribution')
	plt.plot(freq_dist,'*b',linestyle='-')
	plt.ylabel('Percentage occurence')
	plt.savefig(path[:-4]+'.png')

def make_freq_dist_global(path):
	try:
		edge_dist = pd.read_csv(path)
	except Exception, e:
		raise e

	# edge_dist = edge_dist.sort('Interval')
	intervals = [literal_eval(i) for i in list(edge_dist['Interval'])]
	num_edges1 = list(edge_dist['2008-09 (Edges)'])
	num_edges2 = list(edge_dist['2010-11 (Edges)'])
	num_edges3 = list(edge_dist['2012-13 (Edges)'])
	num_edges4 = list(edge_dist['2014-15 (Edges)'])
	num_edges_all = list(edge_dist['2008-15 (Edges)'])
	totals = sum(num_edges1), sum(num_edges2), sum(num_edges3), sum(num_edges4), sum(num_edges_all)
	freq_dist_08 = [float(edge)/totals[0] for edge in num_edges1]
	freq_dist_10 = [float(edge)/totals[1] for edge in num_edges2]
	freq_dist_12 = [float(edge)/totals[2] for edge in num_edges3]
	freq_dist_14 = [float(edge)/totals[3] for edge in num_edges4]
	freq_dist_all = [float(edge)/totals[4] for edge in num_edges_all]

	# Labeling plots
	plt.title('Frequency distribution')
	plt.plot(freq_dist_08,'*b', freq_dist_10,'*r', freq_dist_12,'*g', freq_dist_14,'*y', freq_dist_all, '*m',linestyle='-')
	plt.legend(['2008-2009','2010-2011','2012-2013','2014-2015','2008-2015'])
	plt.ylabel('Percentage occurence')
	plt.xlabel('Edge within interval (i,i+1)')
	plt.savefig(path[:-4]+'.png')

if __name__ == '__main__':
	make_freq_dist(sys.argv[1])
