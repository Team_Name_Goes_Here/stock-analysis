#!/usr/bin/python
# This file will write a csv file that will store the spearman 
# rank correlation coefficients for every pair of stocks

import sys, inspect, csv, os, os.path
import numpy as np
sys.path.insert(0,'../')
from helper import *

pathname = '../DataStripping/'

# Add current dir to search path.
sys.path.insert(0, pathname)
# Add module from the current directory.
sys.path.insert(0, os.path.dirname(os.path.abspath(os.path.realpath(__file__))) + "/DataStripping")

# from Correlations import Correlations as cor
from CSVMethods import get_content

def main():

	# open and write to the Spearman_ADJ.csv file.
	try:
		outputFile = open('../Data/Intersection_ADJ.csv', 'w')
		outputWriter = csv.writer(outputFile)
	except Exception, e:
		raise e

	# List all file names (stockname.csv) in Details folder
	details = "../Data/historicaldata/Details/"
	snames = os.listdir(details)

	if(".~lock.AAC.csv#" in snames):
		snames.remove(".~lock.AAC.csv#")

	stocknames = np.asarray(snames)
	outputWriter.writerow([s[:-4] for s in snames])

	# Make a correlation instance
	# c = cor()
	# For analytics
	count = 0.0
	total = 922*923/2
	shift = 0

	# for each name in stocknames
	for a in xrange(1, len(stocknames)):
		row = []#ow = np.array([])

		# For every other stock related to stocknames[a]
		for b in xrange(a,len(stocknames)):
			# rho  = c.spearman_cor(stocknames[a], stocknames[b])


			row.append(intersection_length(details+stocknames[a], details+stocknames[b]))

			count+=1
			percent = 100*count/total

			print("Complete: %.4f %% on stock %s with stoch %s "% (percent, stocknames[a], stocknames[b]))
			#print row
			#row = np.append(np.array([0 for i in xrange(a-2)]),row)


		print(len(row), stocknames[a])
		#print row
		#exit(1)
		outputWriter.writerow([stocknames[a][:-4]] + [0]*shift + row)
		shift += 1
		

	outputFile.close()

main()
