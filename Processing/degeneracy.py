import numpy as np
import csv

#Given array of 0's and 1's indicating edges where rows and columns are stocks,
#returns array of edges with minimum k degree (done recursively) and indices of stocks
#that belong to that kcore.
def k_core2(data,k):
    row_sums = np.sum(data, axis = 1) #Find degree of each stock.
    col_sums = np.sum(data, axis = 0)

    degree = []
    for i in range(len(row_sums)):
        degree.append(row_sums[i] + col_sums[i]) #-2 if 1 on diagonal
    degree = np.array(degree)

    indices = np.where((degree > 0) & (degree < k)) #Find indices of degree less than k.

    while indices[0].size != 0:
        for i in indices: #For stocks with degree less than k...
            data[:,i] = 0 #Set column to 0.
            data[i,:] = 0 #Set row to 0.

        row_sums = np.sum(data, axis = 1) #Find degree of each stock.
        col_sums = np.sum(data, axis = 0)
        degree = []
        for i in range(len(row_sums)):
            degree.append(row_sums[i] + col_sums[i])
        degree = np.array(degree)
        
        indices = np.where((degree > 0) & (degree < k)) #Find indices of degree less than k and not zero.

    stock_indices_in_kcore = np.where(degree > 0) #Find indices of stocks in this kcore.
    return data, stock_indices_in_kcore[0]

#Use if not upper triangular matrix.
def k_core(data,k):
    row_sums = np.sum(data, axis = 1) #Find degree of each stock.
    indices = np.where(row_sums < k + 1) #Find indices of degree less than k (plus 1 to ignore self correlation).
    while indices[0].size != 0:
        for i in indices: #For stocks with degree less than k...
            data[:,i] = 0 #Set column to 0.
            data[i,:] = 0 #Set row to 0.
        row_sums = np.sum(data, axis = 1) #Find degree of each stock.
        indices = np.where((row_sums != 0) & (row_sums < k + 1)) #Find indices of degree less than k and not zero.
    stock_indices_in_kcore = np.where(row_sums != 0) #Find indices of stocks in this kcore.
    return data, stock_indices_in_kcore[0]

#Use if 2 commas at beginning of csv
def get_matrix(file_name):
    fp = open(file_name)
    matrix = []
    first = True
    for line in fp:
        stripped = line.strip().rstrip(',')
        if stripped:
            if first:
                matrix.append(stripped.split(',')[1:])
                first = False
            else:
                matrix.append(stripped.split(','))

    fp.close()
    matrix = np.array(matrix)
    no_ticker_matrix = matrix[1:,1:].astype(float) #REMOVE TICKERS

    #no_ticker_matrix = matrix.astype(float) #TICKERS ALREADY REMOVED
    return no_ticker_matrix

#Use if 1 comma at beginning of csv
def get_matrix2(filename):
    try:
        adj_file = open(filename)
        adj_reader = csv.reader(adj_file)
    except:
        raise Exception('Could not read file: ',filename)
    adj = np.array(list(adj_reader))
    adj = adj[1:,1:]
    adj = adj.astype(float)
    adj_file.close()
    return adj

#Gets Deneracy and Stocks in it
def degeneracy(file_name, tickers):
    matrix = get_matrix(file_name)
    i = 1
    while True:
        kcore = k_core(matrix,i)
        if not kcore[0].any():
            break
        i += 1
    deg = i - 1

    if deg:
        matrix = get_matrix(file_name)
        indices = k_core(matrix,deg)[1]
        stocks = []
        for i in indices:
            stocks.append(tickers[i])
    else:
        stocks = []

    return deg, stocks


#Gets list of tickers (requires TICKERS.csv)
def get_tickers():
    fp = open('TICKERS.csv')
    tickers = []
    for line in fp:
        stripped = line.strip().rstrip(',')
        if stripped:
            tickers += stripped.split(',')
    fp.close()
    return tickers

def get_path(year, theta):
    fp = year + '/Theta' + theta + '/Logical_ADJ.csv'
    return fp

#This will only work with file in /Thetas
def main():
    tickers = get_tickers()
    data_file = open('kcore_data.csv','w')
    data_file.write("Years, Theta, Degeneracy, Stocks Count, Stocks \n")

    for year in ['2008-9','2010-11','2012-13','2014-15','2008-15']:
        for theta in ['0.1','0.2','0.3','0.4','0.5','0.6','0.7','0.8','0.9','0.95']:
            fp = get_path(year,theta)
            deg, stocks = degeneracy(fp, tickers)
            count = str(len(stocks))
            to_write = year + ',' + theta + ',' + str(deg) + ',' + count + ','
            for stock in stocks:
                to_write += stock + ' '
            to_write += '\n'
            data_file.write(to_write)
            print('Done: ' + year + ' - ' + theta)
    data_file.close()

    # data = get_matrix('Logical_ADJ.csv')
    # kcore = k_core(data, 156)
    # print(kcore[0].any())
    # print(kcore[1])
    # print(len(kcore[1]))


if __name__ == '__main__':
    main()