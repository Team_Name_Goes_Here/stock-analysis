import sys
sys.path.insert(0, "../DataStripping")
from CSVMethods import *

def add(x,y):

	if x != '' and y != '':
		return float(x) + float(y)
	else:
		return ''

if __name__ == "__main__":

	content_prelim = get_content(sys.argv[1])
	labels = content_prelim[0]
	content = [A[1:] for A in content_prelim[1:]]

	transpose = zip(*content)

	new_mat = [[add(x,y) for x, y in zip(content[i], transpose[i])] for i in range(0, len(content))]

	for i in range(0, len(new_mat)):
		new_mat[i][i] = int(new_mat[i][i])/2

	new_mat.insert(0, labels)

	new_mat_list = [list(k) for k in new_mat]

	j = 0
	for m in new_mat_list[1:]:
		m.insert(0, labels[j])
		j += 1

	write_data(new_mat_list, sys.argv[1])
