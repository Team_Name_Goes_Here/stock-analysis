#!/usr/bin/python
# This file will write a csv file that will store the spearman 
# rank correlation coefficients for every pair of stocks

import sys, inspect, csv, os, os.path
import numpy as np

pathname = '../DataStripping/'

# Add current dir to search path.
sys.path.insert(0, pathname)
# Add module from the current directory.
sys.path.insert(0, os.path.dirname(os.path.abspath(os.path.realpath(__file__))) + "/DataStripping")

from Correlations import Correlations as cor
from CSVMethods import get_content

if len(sys.argv) < 2:
	start_year = '2007'
	end_year = '2016'
else:
	start_year = sys.argv[1]
	end_year = sys.argv[2]

def main():

	# open and write to the Spearman_ADJ.csv file.
	try:
		outputFile = open('../Data/AdjacencyMatrices/Spearman_ADJ_'+start_year+'_to_'+end_year+'.csv', 'w')
		outputWriter = csv.writer(outputFile)
	except Exception, e:
		raise e

	# List all file names (stockname.csv) in Details folder
	details = "../Data/historicaldata/Details/"
	snames = os.listdir(details)

	if(".~lock.AAC.csv#" in snames):
		snames.remove(".~lock.AAC.csv#")

	stocknames = np.asarray(snames)
	outputWriter.writerow([s[:-4] for s in snames])

	# Make a correlation instance
	c = cor()

	# For analytics
	count = 0.0
	total = 922*923/2
	shift = 0

	last, next = 0, 0
	# for each name in stocknames
	for a in xrange(2, len(stocknames)):
		row = []

		# For every other stock related to stocknames[a]
		for b in xrange(a,len(stocknames)):
			rho  = c.spearman_cor(details+stocknames[a], details+stocknames[b], start_year, end_year)

			row.append(rho)

			count+=1
			percent = 100*count/total
			next = int(percent)
			if next > last:
				last = next
				print "Percent complete %d /100"%next

			# print("Complete: %.4f %% on stock %s with stoch %s (%.4f)"% (percent, stocknames[a], stocknames[b],rho))
			#print row
			#row = np.append(np.array([0 for i in xrange(a-2)]),row)

		# print(len(row), stocknames[a])
		#print row
		#exit(1)
		outputWriter.writerow([stocknames[a][:-4]] + [0]*shift + row)
		shift += 1
		

	outputFile.close()

main()
