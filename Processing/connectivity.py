import csv, sys
import numpy as np 
from numpy import linalg as LA 
from scipy import linalg as LA1
from pprint import pprint
# import matplotlib.pyplot as plt 

# convert string entries to numbers
def to_num(adj):
	return [[int(float(i)) for i in row] for row in adj]

def zero_diagonal(adj):
	for i in xrange(len(adj)):
		for j in xrange(len(adj)):
			if i == j:
				adj[i][j] = 0
	return np.array(adj)

# First read in the logical matrix
def read_adj(path): 
	try:
		adj_obj = open(path)
		adj = csv.reader(adj_obj)
	except Exception, e:
		raise e
	return list(adj)

# separate header names
def separate_tickers(adj): 	return adj[0], [i[1:] for i in adj[1:]]

# read in the degree vector
def read_deg(path):
	try:
		deg_obj = open(path)
		deg = csv.reader(deg_obj)
	except Exception, e:
		raise e
	return list(deg)

############## NOT NEEDED ANYMORE ##############
# # Make the graph laplacian
# def make_laplacian(adj, deg):
# 	# Make a copy of the adjacency matrix
# 	# and alter it to make the graph laplacian
# 	GL = np.copy(adj)
# 	for i in xrange(len(GL)):
# 		for j in xrange(len(GL)):
# 			# add the degree along the diagonal
# 			if i == j:
# 				GL[i][j] = deg[i]
# 	return GL

# Save it as a .csv file
def save_laplacian(path, matrix):
	with open(path.replace('Logical_ADJ','Graph_Laplacian'),'wb') as f:
		writer = csv.writer(f)
		writer.writerows(matrix)

# REDUNDANT
# # Calculate eigenvalues and return them
# def eigenvalues_of(matrix):		return  LA.eig(matrix)

def make_degree_matrix(adj):
	deg_matrix = np.zeros((len(adj), len(adj)))
	for i in xrange(len(adj)):
		deg_matrix[i,i] = sum(adj[i])
	return deg_matrix

# Find number of connected components and smallest positive eigenvalue
def conn_comp_and_connectivity(e_vals):
	uniq_e_vals = np.unique(e_vals)
	e_val_list = []
	sorted_evals = sorted(e_vals)
	for e in sorted_evals:
		e_val_list.append([k for k in sorted_evals if e == k])

	uniq_lists = list(np.unique(np.asarray(e_val_list)))

	num_conn_comp = len(uniq_lists[0])
	connectivity = uniq_lists[1][0]

	return num_conn_comp, np.real(connectivity)

# Check if a matrix is square
def isSquare (m): return all (len (row) == len (m) for row in m)

# TESTS AND ASSERTIONS
def test_matrices(adj, deg, laplacian):
	# checking stuff
	assert(adj.all() >= 0)
	assert(adj.any() > 0)
	
	# tests
	assert(not deg_matrix.diagonal().any() < 0)
	assert(deg_matrix.diagonal().all() >= 0)
	assert(isSquare(deg_matrix))
	assert(isSquare(adj))
	assert(np.size(deg_matrix, 1) == np.size(adj, 1))
	assert(np.size(deg_matrix, 0) == np.size(adj, 0))
	sum_of_rows = np.array([sum(row) for row in laplacian]) 	# should be all zeros
	assert(sum_of_rows.all() == 0)
	assert(not sum_of_rows.any() != 0)
	assert(laplacian.all() == 0)
	assert((laplacian == laplacian.transpose()).all())
	assert((adj == adj.transpose()).all())

	lap = tuple([tuple(row) for row in laplacian])
	
	# ones vector is a eigen vector with eigen value 0
	flag1 = np.matrix(lap)*np.ones( (len(laplacian), 1) ) == np.zeros( (len(laplacian), 1) )
	flag2 = np.matrix(lap)*np.ones( (len(laplacian), 1) ) == 0*np.ones( (len(laplacian), 1) )
	return flag1.all() and flag2.all()

if __name__ == '__main__':
	# read the adjacency matrix in
	adjancency = read_adj(sys.argv[1])		# takes in a path to logical matrix
	headers, adj = separate_tickers(adjancency)

	# convert to 2D array of integers
	adj = to_num(adj)

	# zero out the diagonal of the adjacency matrix
	adj = zero_diagonal(adj)
	
	# make degree matrix
	deg_matrix = make_degree_matrix(adj)	

	# make the graph laplacian
	lap = deg_matrix - adj
	assert(test_matrices(adj, deg_matrix, lap).all())
	
	# save the file
	save_laplacian(sys.argv[1], lap)

	# get eigenvalues and eigenvectors of laplacian matrix
	e_vals, e_vecs = LA1.eigh(lap)

	assert(not np.iscomplex(e_vals).all())
	assert(e_vals.all() >= 0)

	# determine number of connected components and connectivity  
	print 'Number of connected components is %d \nSmallest positive eigenvalue is %.5f'%conn_comp_and_connectivity(e_vals)
	