import random
import math
import sys
sys.path.insert(0,'../DataStripping')
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from CSVMethods import *

#Given array of 0's and 1's indicating edges where rows and columns are stocks,
#returns array of edges with minimum k degree (done recursively) and indices of stocks
#that belong to that kcore.
def k_core(data,k):
    row_sums = np.sum(data, axis = 1) #Find degree of each stock.
    indices = np.where(row_sums < k + 1) #Find indices of degree less than k (plus 1 to ignore self correlation).
    while indices[0].size != 0:
        for i in indices: #For stocks with degree less than k...
            #print("\nLess than k\n")
            #print(data[i])
            data[:,i] = 0 #Set column to 0.
            data[i,:] = 0 #Set row to 0.
        row_sums = np.sum(data, axis = 1) #Find degree of each stock.
        indices = np.where((row_sums != 0) & (row_sums < k + 1)) #Find indices of degree less than k and not zero.
    stock_indices_in_kcore = np.where(row_sums != 0) #Find indices of stocks in this kcore.
    return data, stock_indices_in_kcore[0]

#Plots graph from adjacency matrix.
def show_graph(adjacency_matrix):
    rows, cols = np.where(adjacency_matrix == 1) #Get edges from adj. matrix.
    edges = zip(rows.tolist(), cols.tolist())
    gr = nx.Graph()
    gr.add_edges_from(edges)
    sub_graphs = list(nx.connected_components(gr)) #Get list of subgraph nodes
    print("Subgraphs")
    print(sub_graphs)
    pos = nx.shell_layout(gr) #Layout of nodes
    nx.draw_networkx(gr,pos, node_color="deepskyblue", edge_alpha = 0.8)
    plt.show()

#Creates random symmetric nxn matrix with 1s on diagonal.
def random_matrix(n):
    data_set = []
    for i in range(n):
        stock = []
        for i in range(n):
            stock.append(random.randint(0,1))
        data_set.append(stock)
    data_set = np.array(data_set)
    data_set = np.triu(data_set) #Zeros lower triangular matrix.
    data_set = data_set + data_set.T #Adds matrix transpose.
    np.fill_diagonal(data_set,1) #Puts 1 on diagonal.
    return data_set

def to_float(f):
    if f == '':
        return 0
    return float(f)

def main():
    # matrix = random_matrix(10)
    # print("Data Set")
    # print(matrix)

    # #Print kcore degree matrix and indices of stocks in that kcore.
    # kcore_matrix,stocks = k_core(matrix,3)
    # print("kCore Matrix")
    # print(kcore_matrix)
    # print("Indices of Stocks in kCore")
    # print(stocks)

    # show_graph(kcore_matrix) #Generates graph

    

    matrix = get_content(sys.argv[1])
    sub_matrix = [[to_float(b) for b in a[1:]] for a in matrix[1:]]
    data_set = np.array(sub_matrix)
    # print(data_set)
    k_core_matrix,stocks = k_core(data_set,33)
    print(stocks)
    print(len(stocks))
    # kcore_list = list([list(k) for k in k_core_matrix])
    # kcore_list.insert(0,list(matrix[0][1:]))

    # for i in range(1,len(matrix[0][1:])):
    #     kcore_list[i].insert(0,matrix[0][i+1])

    #write_data(kcore_list, "../Data/whatever.csv")
    #print(kcore_list)
    show_graph(k_core_matrix)



if __name__ == '__main__':
    main()