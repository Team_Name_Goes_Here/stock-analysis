# This file extract the adjacency matrix whose correlations are
# absolutely greater than the given value
import sys, os, csv
import numpy as np 


# GLOBAL VARIABLES
theta = float(sys.argv[1])


def get_adj(theta):
	# make a directory filled with different adj matrices
	folders = os.listdir('../Data/')
	threshold_matrices = []

	if 'theta' not in folders:
		os.mkdir('../Data/theta')
		print True
	else:
		try:
			matrix_file = open('../Data/theta/theta='+str(theta)+'.csv','rb')
			matrix_reader = csv.reader(matrix_file)
		except Exception, e:
			matrix_file = open('../Data/theta/theta='+str(theta)+'.csv','wb')
			matrix_writer = csv.writer(matrix_file)
			matrix_file.close()
			print os.listdir('../Data/theta/')
			raise Exception('File did not exist, now it does')

		adj = list(matrix_reader)
		return adj

# will populate the matrix with theshold theta
def populate(adj):
	global theta
	# if the adjacency matrix does not have entries
	if not adj:
		print 'empty'
		# read from Adjacency matrix
		try:
			adj_file = open('../Data/Spearman_ADJ_Full.csv')
			adj_reader = csv.reader(adj_file)
		except Exception, e:
			raise Exception('Could not read Spearman_ADJ_Full.csv file')

		try:
			matrix_file = open('../Data/theta/theta='+str(theta)+'.csv','w')
			matrix_writer = csv.writer(matrix_file)
		except Exception, e:
			raise Exception('Could not write to theta='+str(theta)+'.csv')

		adj = np.array(list(adj_reader))[0:-1,0:-1]
		adj = np.array([np.array([float(i) if i != '' else 0.0 for i in row]) for row in adj])
		mask = np.logical_or(adj > theta, adj < -theta)
		
		for x in xrange(1,len(adj)):
			row = adj[x]
			for y in xrange(1,len(row)):
				if np.absolute(adj[x,y]) <= theta:
					adj[x,y] = 0.0

		for row in adj:
			matrix_writer.writerow(row)

		matrix_file.close()

	else:
		print 'theta='+str(theta)+'.csv is not empty'
		response = raw_input('Want to overwrite? ')
		if response in ['y', 'yes']:
			adj = []
			populate(adj)
		else:
			print 'overwrite cancelled'


def main(theta):
	adj = get_adj(theta)
	populate(adj)
	
if __name__ == '__main__':
	main(theta)

