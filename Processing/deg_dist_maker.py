import matplotlib.pyplot as plt 
import numpy as np 
import sys, csv

# Given a csv file that has the degree vector,
# create the degree distribution 

# This reads the csv file that contains the degree of our matrix
def read_vector(path):
	try:
		deg_vec = open(path)
	except Exception, e:
		raise e

	deg_vec_reader = csv.reader(deg_vec)

	content =  list(deg_vec_reader)

	return [int(a[1]) for a in content[1:]]

# This creates and saves a file that contains the
# degree distribution of our matrix
def make_deg_dist(path):
	deg_vec = read_vector(path)

	sorted_vec = sorted(deg_vec)
	total = sum(deg_vec)
	uniq_vecs = np.unique(deg_vec)
	
	deg_lists = []
	for i in sorted_vec:
		deg_lists.append([k for k in sorted_vec if k == i])

	uniq_lists = list(np.unique(np.asarray(deg_lists)))
	freq_list = [float(len(i))/total for i in uniq_lists]
	plt.plot(uniq_vecs, freq_list, '*b', linestyle='-')
	plt.title('Degree distribution')
	plt.xlabel('Degree')
	plt.ylabel('Percentage occurence')
	plt.savefig(path[:-4]+'.png')



if __name__ == '__main__':
	make_deg_dist(sys.argv[1])

