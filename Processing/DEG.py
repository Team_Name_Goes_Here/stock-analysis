import numpy as np 
import csv
import sys

# This script will take in an adjancency matrix
# it will output a degree vector
def main(adjancency_matrix_file_name='Spearman_ADJ_Full.csv'):
	# read in a matrix row by row
	matrix_file = open('../Data/'+adjancency_matrix_file_name)
	matrix_reader = csv.reader(matrix_file)

	# make a count to for each row
	degree = np.array([])

	for row in matrix_reader:
		count = 0
		for entry in row:
			if entry != '':
				count+=1
		degree = np.append(degree, count)
	
	print degree
	# write to a file the degree matrix.
	deg_matrix = open('../Data/Spearman_DEG.csv','w')
	matrix_writer = csv.writer(deg_matrix)
	matrix_writer.writerow(degree)

main(sys.argv[1])