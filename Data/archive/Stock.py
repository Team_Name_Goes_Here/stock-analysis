class Stock(object):

	def __init__(self,name, volList, priceList):
		self.vol = volList
		self.price = priceList
		self.name = name
	
	def get_vol_list(self):
		return self.vol
	
	def get_price_list(self):
		return self.price

	def get_name(self):
		return self.name

	def equals(self,S):
		return (set(S.get_vol_list()) == set(self.vol) and set(S.get_price_list()) == set(self.price) and S.get_name() == self.name)
