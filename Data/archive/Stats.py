class Stats:

	def std_dev(list):
		avg = sum(list)/len(list)

		temp = {x - avg : x in list}
		squares = {x**x : x in temp}

		return sum(squares)
