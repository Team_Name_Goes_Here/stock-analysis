import numpy as np

def len_distribution(matrix, stock = None):
    if stock:
        position = np.where(matrix == stock)[0][1]
        count_matrix_1 = np.reshape(matrix[position:position + 1], matrix[position:position + 1].size)
        count_matrix_1[position] = 0
        count_matrix_2 = np.reshape(matrix[:,position:position + 1],matrix[:,position:position + 1].size)
        count_matrix = np.append(count_matrix_1[1:], count_matrix_2[1:])
    else:
        count_matrix = np.reshape(matrix[1:,1:],matrix[1:,1:].size) #Convert to 1D matrix without tickers
    
    count_matrix = count_matrix.astype(int) #Convert matrix values to ints
    counts = np.bincount(count_matrix)

    if stock:
        wf = open(stock + "_Len_Dist.csv",'w')
    else:
        wf = open(file_name[:-4] + "_Len_Dist.csv",'w')
    total = np.count_nonzero(count_matrix)
    wf.write('Length, Count, Sum (Counts <= Length), Percent of Nonzero Data, Nonzero Data Count = ' + str(total) + ', Zero Data Count = ' + str(counts[0]) + '\n')
    for i in range(1,len(counts)):
        count_sum = sum(counts[1:i+1]) #Sum nonzero counts.
        percent = (count_sum / total) * 100
        wf.write(str(i) + ',' + str(counts[i]) + ',' + str(count_sum) + ',' + str(round(percent,2))+'%\n')
    wf.close()

def get_matrix(file_name):
    fp = open(file_name)
    matrix = []
    for line in fp:
        stripped = line.strip().rstrip(',')
        if stripped:
            matrix.append(stripped.split(','))
    fp.close()
    matrix = np.array(matrix)
    return matrix

def main():
    matrix = get_matrix('Intersection_ADJ.csv')
    len_distribution(matrix)
    # for ticker in matrix[:1][0][1:]:
    #     print(ticker)
    #     len_distribution(matrix, stock = ticker)


if __name__ == '__main__':
    main()



